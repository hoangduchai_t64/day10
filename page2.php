<!DOCTYPE html>
<html lang="en">
<?php
$question = array(
    "6. Bộ phận nào của cơ thể tiêu thụ oxy nhiều nhất?" => "A. Phổi|B. Não|C. Da|D. Tim", 
    "7. Nghị định Kyoto là một thỏa thuận giữa các nước về vấn đề gì?" => "A. Thương mại|B. Môi trường|C. Bản quyền|D. Quân sự",
     "8. Rượu làng Vân nổi tiếng có nguồn gốc từ đâu?" => "A. Ninh Bình|B. Yên Bái|C. Bắc Ninh|D. Bắc Giang",
     "9. Vị trạng nguyên nào thời nhà Trần được phong là \"Lưỡng quốc trạng nguyên\"?" => "A. Nguyễn Trực|B. Mạc Đĩnh Chi|C. Nguyễn Bỉnh Khiêm|D. Đặng Công Chất",
      "10. Québec là thành phố ở nước nào?" => "A. Mexico|B. Cuba|C. Canada|D. Nam Phi"
);
$json = json_encode($question);
setcookie("cookie[page2]",$json);
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="page12.css">
</head>

<body>
    <div class="main-content">
        <?php
        if (isset($_POST["btn-submit"])) {
            saveData();
            header('location: page3.php');
        }
        if (isset($_POST["btn-back"])) {
            saveData();
            header('location: page1.php');
        }
        function saveData(){
            $answer_post1 ="answer";
            if(isset($_POST[$answer_post1])){
                $count = 6;
                foreach($_POST[$answer_post1] as $option_num => $option_val){
                    $answer = explode(".",$option_val)[0];
                    setcookie("cookie[$count]",$answer);
                    $count += 1;
                }
            }
    }
        ?>
        <h2>Hoàn thành các câu trắc nghiệm sau</h2>
        <div class="main-question">
            <form method="POST" action="">

                <?php
                $count = 6;
                foreach ($question as $key => $value) {
                    echo "<p>$key</p>";
                    $answer = explode("|", $value);
                    
                    foreach ($answer as $x) {
                        echo "<label>
                        <input id=\"answer\" type=\"radio\" name=\"answer[$count]\" value=\"$x\"";
                        if(isset($_COOKIE['cookie'])){
                            foreach ($_COOKIE['cookie'] as $name => $value) {
                               $keyAnswer = explode(".",$x)[0];
                               if($name == $count && $value == $keyAnswer){
                                   echo "checked";
                               }
                            }
                        }
                        echo" >$x</label>";
                        echo "</br>";
                    }
                    $count += 1;
                }
                ?>

                <input type="submit" value="Nộp bài" id="btn-next" name="btn-submit">
                <input type="submit" value="Back" id="btn-back" name="btn-back">
            </form>
        </div>
    </div>
</body>

</html>