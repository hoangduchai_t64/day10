<!DOCTYPE html>
<html lang="en">
<?php

$question = array("1. Trong câu thành ngữ quen thuộc, cá trê đã chui vào đâu?" => "A. Cống|B. Rãnh|C. Ống|D. Túi"
,"2. Màu chủ đạo của tờ tiền polime mệnh giá 500.000 đồng là gì?" => "A. Đỏ|B. Xanh|C. Vàng|D. Tím"
,"3. Bảo tàng Hồ Chí Minh thiết kế theo hình loại hoa nào?" => "A. Hoa sen|B. Hoa hướng dương|C. Hoa hồng|D. Hoa đào"
,"4. Tác phẩm \"Số đỏ\" của tác giả nào?" => "A. Ngô Tất Tố|B. Vũ Trọng Phụng|C. Nam Cao|D. Nguyễn Công Hoan"
,"5. Quốc gia nào sau đây không giáp biển?" => "A. Lào|B. Việt Nam|C. Singapore|D. Trung Quốc");
$json = json_encode($question);
setcookie("cookie[page1]",$json);
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="page12.css">
</head>

<body>
    <label >
    <?php
        if(isset($_POST["btn-next"])){
            saveData();
            header('location: page2.php');
        }

        function saveData(){
                $answer_post1 ="answer";
                if(isset($_POST[$answer_post1])){
                    $count = 1;
                    foreach($_POST[$answer_post1] as $option_num => $option_val){
                        $answer = explode(".",$option_val)[0];
                        setcookie("cookie[$count]",$answer);
                        $count += 1;
                    }
                }
        }
        
       
    ?>
    </label>
   
    <div class="main-content">
        <h2>Hoàn thành các câu trắc nghiệm sau</h2>
        <div class="main-question">
            <form method="POST" action="">
                <?php
                $count = 1;
                foreach ($question as $key => $value) {
                    echo "<p>$key</p>";
                    $answer = explode("|", $value);
                    
                    foreach ($answer as $x) {
                        echo "<label>
                        <input id=\"answer\" type=\"radio\" name=\"answer[$count]\" value=\"$x\"";
                        if(isset($_COOKIE['cookie'])){
                            foreach ($_COOKIE['cookie'] as $name => $value) {
                               $keyAnswer = explode(".",$x)[0];
                               if($name == $count && $value == $keyAnswer){
                                   echo "checked";
                               }
                            }
                        }
                        echo" >$x</label>";
                        echo "</br>";
                    }
                    $count += 1;
                }
                ?>

                <input type="submit" value="Next" id="btn-next" name="btn-next">
            </form>
        </div>
    </div>
</body>

</html>