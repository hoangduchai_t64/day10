<!DOCTYPE html>
<html lang="en">
<?php
$answerTotal = array(1 => "C", 2 => "B", 3 => "A", 4 => "B", 5 => "A", 6 => "B", 7 => "B", 8 => "D", 9 => "B", 10 => "C");
$point = 0;
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="page3.css">
</head>

<body>
    <?php
    if (isset($_GET["id"])) {  
        echo "hhh";
    }
    ?>

    <div class="main-content">
        <div class="list-answer">

            <?php
            foreach ($answerTotal as $number => $value) {
                echo "<div id = \"box-number\" ";

                if (isset($_COOKIE['cookie']) && isset($_POST['btn-submit'])) {
                    foreach ($_COOKIE['cookie'] as $name => $data) {
                        if ($number == $name && $data == $answerTotal[$number]) {
                            echo "style=\"background-color:#76ff03;\"";
                            $point += 1;
                        }
                    }
                }
                echo ">
                    $number
                    </div>";
            }
            ?>

        </div>
        <div id="point">
            <?php
            if (isset($_POST["btn-submit"])) {
                echo "<p> Điểm của bạn là : $point";

                if ($point < 4) {
                    echo " Bạn quá kém, cần ôn tập thêm";
                } elseif ($point < 7 && $point > 4) {
                    echo " Cũng bình thường";
                } else {
                    echo " Sắp sửa làm được trợ giảng lớp PHP";
                }
                echo "</p>";
            }

            ?>
        </div>
        <div class="submit-answer">
            <form method="POST" action="">


                <input type="submit" value="Xem Điểm" id="btn-submit" name="btn-submit">
            </form>
        </div>

        <div class="list-questions">

            <?php
            if (isset($_POST['btn-submit']) && isset($_COOKIE['cookie'])) {
                $count = 1;
                foreach ($_COOKIE['cookie'] as $name => $value) {
                    if (count($_COOKIE['cookie']) > 2) {
                        if ($name == "page1" || $name == "page2") {
                            $savedListQuestionPage1 = json_decode($value, true);
                            foreach ($savedListQuestionPage1 as $question => $listAnswer) {
                                echo " <p id = \"question\">$question</p>";
                                $answer = explode("|", $listAnswer);
                                foreach ($answer as $x) {
                                    echo "<label >";
                                    foreach ($_COOKIE['cookie'] as $key => $myAnswer) {
                                        if ($key != "page1" && $key != "page2") {
                                            $keyAnswer = explode(".", $x)[0];
                                            if ($key == $count && $keyAnswer == $answerTotal[$key]) {
                                                echo "<font color=76ff03>";
                                            }
                                            if ($key == $count && $myAnswer == $keyAnswer) {
                                                if ($myAnswer != $answerTotal[$key]) {
                                                    echo "<font color=dd2c00>";
                                                }
                                            }
                                        }
                                    }
                                    echo  "<span class= \"dot\"";
                                    foreach ($_COOKIE['cookie'] as $key => $myAnswer) {
                                        if ($key != "page1" && $key != "page2") {
                                            $keyAnswer = explode(".", $x)[0];
                                            if ($key == $count && $keyAnswer == $answerTotal[$key]) {
                                                echo "style=\"background-color:#76ff03;\"";
                                            }
                                            if ($key == $count && $myAnswer == $keyAnswer) {
                                                if ($myAnswer != $answerTotal[$key]) {
                                                    echo "style=\"background-color:#dd2c00;\"";
                                                }
                                            }
                                        }
                                    }
                                    echo "></span>$x</font></label>";
                                    echo "</br>";
                                }
                                $count += 1;
                            }
                        }
                    }
                }
            }
            ?>
        </div>

    </div>
</body>

</html>